# simple-java-springboot-template

## Overview

Template for Spring Boot app originally created from [Spring Initializr](https://start.spring.io)
website and copied from this [repo](https://codeberg.org/fionahiklas/simple-java-springboot-template)

## References

### Apache

* [Ant Ivy getting started](https://ant.apache.org/ivy/history/latest-milestone/tutorial/start.html)
* [Ant tutorial](https://ant.apache.org/manual/tutorial-HelloWorldWithAnt.html)
* [Ant echo properties](https://ant.apache.org/manual/Tasks/echoproperties.html)
* [Ivy build file](https://github.com/apache/ant-ivy/blob/master/build.xml)
* [Ivy retrieve task](https://ant.apache.org/ivy/history/2.2.0/use/retrieve.html)
* [Antlib task definitions](https://ant.apache.org/manual/Types/antlib.html)
* [Ivy XSD](https://ant.apache.org/ivy/schemas/ivy.xsd)


### Java

* [JUnit 5](https://junit.org/junit5/docs/current/user-guide/)
* [JUnit Jupiter Maven Central](https://mvnrepository.com/search?q=org.junit.jupiter)
* [Spring static class configurer](https://github.com/thombergs/code-examples/blob/master/spring-boot/spring-boot-testconfiguration/src/test/java/io/reflectoring/springboot/testconfiguration/UsingStaticInnerTestConfiguration.java)
* [TestConfiguration annotation](https://reflectoring.io/spring-boot-testconfiguration/)








