package com.hiklas.template.springboot.ant;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;


@SpringBootTest
class SpringBootTemplateProjectApplicationTests {
    
    @TestConfiguration
    public static class DummyConfiguration {

    }
    
    @Test
    void contextLoads() {
	System.err.println("HELLO THERE!");
	assertTrue(true);
    }

    @Test
    void testSomethingElse() {
	System.err.println("HELLO THERE TOO!");
	assertFalse(false);
    }

}
